# Devops Exercise for New10

Under the `infra` folder you will find a Terraform module that creates an ECS service than runs a simple nginx website, and a deployment to eu-west-1 region of that module.

We would like you to turn this simple module into an internet facing service with an active-active multi-region deployment.

Some more hints (to make things better):
* Feel free to use Terraform, TypeScript, Python 
* Think about what you think we want to achieve with:
  * an active-active setup, 
  * what other capabilities we need
  * what could go wrong and how to prevent / cope with this
  * and design your solution around these things!
* Document your thoughts!

Bonus points:
* Add a CICD pipeline for this


# Notes

## CND

For this implementation assumption is that we are hosting a "simple nginx website". This means that 90%+ of the requests should in theory be 'GET' requests and should be cacheable. Due to the fact that this is a website, it is highly recommended to have a content delivery network to scale easily.

For this purpose I am going to go with implementation that relies on Cloudfront on the edge.
Cloudfront unfortunately does not offer active-active setup where traffic is routed with different routing strategies such as weighted, geolocation, latency routing.

We could have achieved this with using Route53 DNS routing policy and using ALB's on the edge, or using AWS Global Accelerator with Anycast IP implementation (my prefference). This in my opinion would not be a suitable solution, unless we plan to host content that is not cacheable, or if we plan to use AWS API gateway, which already supports caching on the edge. All of this is more suitable for hosting of headless applications but not static content such as simple websites.

## Load Balancer & SSL Termination
I chose to go with ALB, simply because I want to outsource TLS termination to AWS. This will decrease maintenance cost. I would also even question whether NGNX can be removed as a component, and whether some other solutions like S3 hosting would be sufficient if it is a simple static website. 

Another reason I like ALB, is the flexilibity we will have in the future. We have option to use listener rules and control routing based on paths, headers, request methods, query strings and Source IP. This flexibility allows us to have much more control for our routing out of the box, if needed in the future. 


Another potential solution would also be NLB, but in this case, our proxy would be on the edge, and we would be in charge of managing the TLS termination. For example, an improvement for this implementation (ALB) could be a authenticated origin pulls configuration trough the use of auth header.With NLB for example, this is not possible. You can use security groups to whitelist all of the Cloudfront public IP ranges, but is very hacky solution due to the ammount of CIDR's (not everything fits in single sec group), and again these can change and you would need an automated mechanism to keep the up to date. This increases complexity.

## Compute
For hosting our containers, I decided to go with Fargate, because it is serverless. By default it is secured and very elastic.

## High level architecture diagram
 ![image](https://drive.google.com/uc?export=view&id=1nkGjw5rXdiBsVw3QGhNUq9x0wT_GD_VS)


 # Improvements
* Authenticated Origin Pulls
* Observability and Alerting with Cloudwatch
* Automated cache invalidation
* Using S3 as state and Dynamodb as lock, less prone to errors in bigger teams. 
* Maybe consider other CDN's if fully active-active is required. Ie. Cloudflare offers GLB + Caching edge with intelligent routing. I think Akamain as well. 
* Tagging all of the resources. 
* Releasing & versioning modules. Setting up DTAP (please see diagram below)
* Build kitchen tests for module
* Generally iprove readme for each module & variable descriptions.
* Dynamic CIDRs & AZ-s for given region.
* Use IAM roles for provider authentication
* Use Terragrunt and simplify the folder structure and keep both modules and provider code DRY.

## CICD Flow (One of many possible solutions)
 ![image](https://drive.google.com/uc?export=view&id=1S5gedz6JIPDMooyLpdD8acIsJEBmcd2M)
