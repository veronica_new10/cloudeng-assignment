
variable "primary_origin" {
  type = string
}

variable "secondary_origin" {
  type = string
}

variable "origin_http_port" {
  type    = number
  default = 8081
}

variable "origin_https_port" {
  type    = number
  default = 4333
}

variable "domain" {
  type = string
}

variable "zone_id" {
  type = string
}