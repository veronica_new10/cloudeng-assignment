resource "aws_route53_record" "cname" {
  zone_id = var.zone_id
  name    = var.domain
  type    = "CNAME"
  ttl     = "300"
  records = [aws_cloudfront_distribution.website.domain_name]
}