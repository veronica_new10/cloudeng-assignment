# We are creating public zone, however it is important to note
# that we have already made NS records in the primary registry that allow us to manage this zone
resource "aws_route53_zone" "this" {
  name = var.domain
}