# VPC Specific
variable "vpc_name" {
  type = string
}

variable "vpc_cidr" {
  type = string
}

variable "private_subnets" {
  type = list(string)
}

variable "public_subnets" {
  type = list(string)
}

# ECS Specific
variable "cluster_name" {
  type = string
}

variable "fargate_cpu_reservation" {
  type    = number
  default = 256
}

variable "fargate_ram_reservation" {
  type    = number
  default = 512
}
variable "image" {
  type = string
}

variable "image_tag" {
  type = string
}

variable "container_cpu_reservation" {
  type    = number
  default = 128
}

variable "container_ram_reservation" {
  type    = number
  default = 256
}

variable "container_port" {
  type    = number
  default = 8080
}

variable "service_desired_count" {
  type    = number
  default = 3
}

variable "deployment_minimum_healthy_percent" {
  type    = number
  default = 50

}
variable "deployment_maximum_percent" {
  type    = number
  default = 200
}

variable "autoscaling_min_capacity" {
  type    = number
  default = 1

}

variable "memory_utilisation_target" {
  type    = number
  default = 80

}

variable "cpu_utilisation_target" {
  type    = number
  default = 60

}

variable "autoscaling_max_capacity" {
  type    = number
  default = 3

}
variable "health_check_grace_period_seconds" {
  type    = number
  default = 60
}

variable "launch_type" {
  type    = string
  default = "FARGATE"

}
variable "scheduling_strategy" {
  type    = string
  default = "REPLICA"
}

variable "assign_public_ip" {
  type    = bool
  default = false

}
variable "lb_enable_deletion_protection" {
  type    = bool
  default = false
}

variable "health_check_path" {
  type    = string
  default = "/"
}

variable "lb_http_port" {
  type    = number
  default = 8081
}

variable "lb_https_port" {
  type    = number
  default = 4333
}

variable "zone_id" {
  type = string
}

variable "domain" {
  type = string
}