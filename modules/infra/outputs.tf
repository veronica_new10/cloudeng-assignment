# VPC
output "vpc_id" {
  value = module.vpc.vpc_id
}
output "lb_dns" {
  value = aws_route53_record.albcname.name
}
output "lb_http_port" {
  value = var.lb_http_port
}
output "lb_https_port" {
  value = var.lb_https_port
}
output "certificate_id" {
  value = aws_acm_certificate.lb.id
}

