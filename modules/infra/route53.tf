
resource "aws_acm_certificate" "lb" {
  domain_name       = format("%s.%s", var.cluster_name, var.domain)
  validation_method = "DNS"
}

resource "aws_route53_record" "lb" {
  for_each = {
    for dvo in aws_acm_certificate.lb.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = var.zone_id
}

resource "aws_acm_certificate_validation" "lb" {
  certificate_arn         = aws_acm_certificate.lb.arn
  validation_record_fqdns = [for record in aws_route53_record.lb : record.fqdn]
}

resource "aws_route53_record" "albcname" {
  zone_id = var.zone_id
  name    = format("%s.%s", var.cluster_name, var.domain)
  type    = "CNAME"
  ttl     = "300"
  records = [aws_lb.main.dns_name]
}