# Fetch available AZ's
data "aws_availability_zones" "available" {}

# Provision a VPC for EKS Cluster
module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.6.0"

  name = var.vpc_name
  cidr = var.vpc_cidr
  azs  = data.aws_availability_zones.available.names

  # Spin up 3 public and private subnets
  private_subnets = var.private_subnets
  public_subnets  = var.public_subnets

  # Due to the fact that this is a testing cluster we will keep it simple and cheap
  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true
}