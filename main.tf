# Configure http backed (gitlab native integration is enabled)
terraform {
  backend "http" {
  }
}

provider "aws" {
    region = "us-east-1"
}

provider "aws" {
    alias = "ireland"
    region = "eu-west-1"
}

module "dns" {
    source = "./modules/dns/"
    providers = {
        aws = aws
    }
    domain = "dev.namiktest.com"
}


module "eu-west-1" {
    source = "./modules/infra/"
    providers = {
        aws = aws.ireland
    }

    vpc_name   = "eu-west-1-vpc-01"
    vpc_cidr = "11.0.0.0/16"
    private_subnets      = ["11.0.1.0/24", "11.0.2.0/24", "11.0.3.0/24"]
    public_subnets       = ["11.0.4.0/24", "11.0.5.0/24", "11.0.6.0/24"]

    cluster_name = "eu-west-1-ecs-01"
    image = "bitnami/nginx"
    image_tag = "latest"

    # Configuration for ALB SSL
    domain = "test.dev.namiktest.com"
    zone_id = module.dns.zone_id
}

module "us-east-1" {
    
    source = "./modules/infra/"
    providers = {
        aws = aws
    }
    # Make VPC unique
    vpc_name = "us-east-1-vpc-01"
    vpc_cidr = "10.0.0.0/16"
    private_subnets      = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
    public_subnets       = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]

    # Configure ecs 
    cluster_name = "us-east-1-ecs-01"
    image = "bitnami/nginx"
    image_tag = "latest"
     
    # Configuration for ALB SSL
    domain = "test.dev.namiktest.com"
    zone_id = module.dns.zone_id
}

module "cdn" {
    source = "./modules/cdn/"
    providers = {
        aws = aws
    }
    primary_origin = module.eu-west-1.lb_dns 
    secondary_origin = module.us-east-1.lb_dns
    
    # Cloudfront domain configuration
    domain = "test.dev.namiktest.com"
    zone_id = module.dns.zone_id
}